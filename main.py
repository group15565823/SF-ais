import pymysql

# установление соединения с базой данных
connection = pymysql.connect(host='mysql-rfam-public.ebi.ac.uk',
                             user='rfamro',
                             password='',
                             database='Rfam',
                             port=4497)

try:
    # создание объекта курсора
    with connection.cursor() as cursor:
        # выполнение запроса SHOW TABLES для получения списка всех таблиц в базе данных "Rfam"
        sql_query = "SHOW TABLES"
        cursor.execute(sql_query)

        # получение результатов запроса
        result = cursor.fetchall()
        print(result)
        
        # проверка наличия таблицы rfam_acc в выводе
        table_exists = False
        for row in result:
            if 'rfam_acc' in row:
                table_exists = True
                break
        
        # создание файла и запись результата проверки
        with open('looking-for-rfam_acc', 'w') as f:
            if table_exists:
                f.write('table rfam_acc exists')
            else:
                f.write('table rfam_acc does NOT exist')
finally:
    # закрытие соединения с базой данных
    connection.close()
